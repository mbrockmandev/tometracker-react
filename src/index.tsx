import * as React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import App from "./App";
import LoginForm from "./components/Public/LoginForm";
import BrowseBooks from "./components/Screens/User/BrowseBooks";
import "./styles/tailwind.css";
import RegisterForm from "./components/Public/RegisterForm";
import NotFound from "./components/Public/NotFound";
import BookDetails from "./components/Book/BookDetails";
import AdminDashboard from "./components/Screens/Staff/Admin/AdminDashboard";
import StaffDashboard from "./components/Screens/Staff/StaffDashboard";
import UserDashboard from "./components/Screens/User/UserDashboard";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <NotFound />,
    children: [
      {
        path: "login",
        element: <LoginForm />,
      },
      {
        path: "register",
        element: <RegisterForm />,
      },
      {
        path: "books",
        element: <BrowseBooks />,
      },
      {
        path: "books/:bookId",
        element: <BookDetails />,
      },
      {
        path: "admin/dashboard",
        element: <AdminDashboard />,
      },
      {
        path: "staff/dashboard",
        element: <StaffDashboard />,
      },
      {
        path: "users/dashboard",
        element: <UserDashboard />,
      },
    ],
  },
]);

root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
