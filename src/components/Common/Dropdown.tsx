import React, { useEffect, useState } from "react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";

const Dropdown = ({ items }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [icon, setIcon] = useState("bars");
  useEffect(() => {
    const handleMouseDownOutsideMenu = (e: MouseEvent) => {
      if (isMenuOpen && e.target !== e.currentTarget) {
        setIsMenuOpen(false);
        setIcon("bars");
      }
    };

    document.addEventListener("mousedown", handleMouseDownOutsideMenu);

    return () => {
      document.removeEventListener("mousedown", handleMouseDownOutsideMenu);
    };
  }, [isMenuOpen]);

  const toggleMenu = () => {
    setIsMenuOpen((cur) => !cur);
    setIcon((cur) => (cur === "bars" ? "xmark" : "bars"));
  };

  return (
    <div>
      <div className="relative mr-2 lg:hidden">
        <div className="inline-flex items-center overflow-hidden rounded-md border bg-white">
          <button
            className="border-e px-4 py-2 text-sm text-gray-600 hover:bg-gray-50 hover:text-gray-700 focus:outline-none"
            onClick={toggleMenu}
          >
            <span className={`myicon`}>
              {icon === "xmark" && (
                <XMarkIcon className={`h-5 w-5 rotate-fade`} />
              )}
              {icon === "bars" && (
                <Bars3Icon className={`h-5 w-5 rotate-fade-in `} />
              )}
            </span>
          </button>
        </div>

        <div
          className={`absolute end-0 z-10 mt-2 w-56 rounded-md border border-gray-100 bg-white shadow-lg fade-in ${
            isMenuOpen ? "" : "hidden"
          }`}
          role="menu"
        >
          <div className="p-2">
            {items.map((item) => (
              <MenuItem label={item.label} icon={item.icon} key={item.label} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

const MenuItem = ({ label, icon }) => {
  return (
    <div>
      <a
        href="/books"
        className="block rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
        role="menuitem"
      >
        {label}
      </a>
    </div>
  );
};

export default Dropdown;
