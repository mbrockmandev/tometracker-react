import React, { useContext } from "react";
import {
  Navbar,
  Collapse,
  Typography,
  Button,
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
} from "@material-tailwind/react";
import {
  CubeTransparentIcon,
  UserCircleIcon,
  CodeBracketSquareIcon,
  ChevronDownIcon,
  PowerIcon,
} from "@heroicons/react/24/outline";
import Dropdown from "./Dropdown";
import { BookOpenIcon } from "./Footer";
import { useNavigate } from "react-router-dom";
import { UserContext, UserContextType, useJwtToken } from "../../App";

function NavMenu() {
  const navigate = useNavigate();

  const { jwtToken, setJwtToken, toggleRefresh } = useJwtToken();
  const { user, setUser } = useContext<UserContextType>(UserContext);
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const menuItemClick = (e) => {
    const target = e.target as HTMLElement;
    const label = target.innerText;
    if (label === "My Dashboard") {
      // TODO: route to dashboard
    }
    if (jwtToken === "") {
      // TODO: route to help
      navigate("/login");
    }
    if (jwtToken !== "") {
      // TODO: check this function
      const logout = () => {
        const requestOptions: RequestInit = {
          method: "GET",
          credentials: "include",
        };
        fetch(`${process.env.REACT_APP_BACKEND}logout`, requestOptions)
          .catch((err) => {
            console.log("error logging out" + err);
          })
          .finally(() => {
            setJwtToken("");
            toggleRefresh(false);
          });
        navigate("/login");
      };
      logout();
    }
  };

  return (
    <Menu
      open={isMenuOpen}
      handler={setIsMenuOpen}
      placement="bottom-end">
      <MenuHandler>
        <Button
          variant="text"
          color="blue-gray"
          className="flex items-center gap-1 py-0.5 pr-2 pl-0.5 lg:ml-auto">
          Menu
          <ChevronDownIcon
            strokeWidth={2.5}
            className={`h-3 w-3 transition-transform ${
              isMenuOpen ? "rotate-180" : ""
            }`}
          />
        </Button>
      </MenuHandler>
      <MenuList className="p-1">
        {/* user dashboard item */}
        <MenuItem
          onClick={menuItemClick}
          className={`flex items-center gap-2 rounded hover:bg-green-500/10 focus:bg-green-500/10 active:bg-green-500/10`}>
          {React.createElement(UserCircleIcon, {
            className: `h-4 w-4`,
            strokeWidth: 2,
          })}
          <Typography
            as="span"
            variant="lead"
            className="text-md py-2"
            color="inherit">
            My Dashboard
          </Typography>
        </MenuItem>
        {/* login */}
        {!jwtToken && (
          <MenuItem
            onClick={menuItemClick}
            className={`flex items-center gap-2 rounded hover:bg-green-500/10 focus:bg-green-500/10 active:bg-green-500/10
          `}>
            {React.createElement(PowerIcon, {
              className: `h-4 w-4`,
              strokeWidth: 2,
            })}
            <Typography
              as="span"
              variant="lead"
              className="text-md py-2"
              color="inherit">
              Login
            </Typography>
          </MenuItem>
        )}
        {/* logout */}
        {jwtToken && (
          <MenuItem
            onClick={menuItemClick}
            className={`flex items-center gap-2 rounded hover:bg-red-500/10 focus:bg-red-500/10 active:bg-red-500/100
            }`}>
            {React.createElement(PowerIcon, {
              className: `h-4 w-4 text-red-500`,
              strokeWidth: 2,
            })}
            <Typography
              as="span"
              variant="lead"
              className="text-md py-2"
              color="red">
              Logout
            </Typography>
          </MenuItem>
        )}
      </MenuList>
    </Menu>
  );
}

// nav list component
const navListItems = [
  {
    label: "Books",
    icon: BookOpenIcon,
  },
  {
    label: "Libraries",
    icon: CubeTransparentIcon,
  },
  {
    label: "FAQ",
    icon: CodeBracketSquareIcon,
  },
];

function NavList() {
  return (
    <ul className="mb-4 mt-2 flex flex-col gap-2 md:mb-0 md:mt-0 md:flex-row md:items-center">
      {navListItems.map(({ label, icon }, key) => (
        <Typography
          key={label}
          as="a"
          href="#"
          variant="small"
          color="blue-gray"
          className="font-normal hover:bg-green-200 focus:bg-green-200/10 active:bg-teal-500/10">
          <MenuItem className="flex items-center gap-2 md:rounded-full">
            {React.createElement(icon, { className: "h-[18px] w-[18px]" })}{" "}
            {label}
          </MenuItem>
        </Typography>
      ))}
    </ul>
  );
}

export default function Header() {
  const [isNavOpen, setIsNavOpen] = React.useState(false);

  React.useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 768 && setIsNavOpen(false),
    );
  }, []);

  return (
    <Navbar
      className="mx-auto max-w-screen-xl my-2 p-2 lg:rounded-full lg:pl-6
       bg-teal-600 bg-gradient-to-r from-teal-400 to-teal-200 text-gray-500
      ">
      <div className="relative flex items-center justify-between">
        <Typography
          as="a"
          href="/"
          className="text-2xl mr-4 ml-2 text-zinc-100 cursor-pointer py-1.5
          hover:shimmer-text
          hover:text-transparent
          hover:bg-gradient-to-r
          hover:from-transparent
          hover:via-white
          hover:to-transparent
          hover:background-clip-text">
          TomeTracker
        </Typography>
        <div className="hidden lg:flex">
          <NavList />
        </div>
        <div className="flex items-center">
          <Dropdown items={navListItems} />
          <NavMenu />
        </div>
      </div>
      <Collapse
        open={isNavOpen}
        className="overflow-scroll text-gray-500">
        <NavList />
      </Collapse>
    </Navbar>
  );
}
