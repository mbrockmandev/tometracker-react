import React, { useEffect, useState } from "react";
import { useJwtToken } from "../../../App";
import BorrowedBooks from "../../Book/BorrowedBooks";

const UserDashboard = () => {
  const { jwtToken } = useJwtToken();
  const [borrowedBooks, setBorrowedBooks] = useState(null);

  useEffect(() => {}, []);

  if (jwtToken === "") {
    return;
  }

  return (
    <div>
      <div className="library-dashboard flex">
        <div className="main-section flex-1 p-4 space-y-4">
          {/* Search Bar */}
          <div className="search-bar relative">
            <input
              type="text"
              placeholder="Search..."
              className="mx-auto w-[35%] px-4 py-2 border rounded focus:outline-none focus:ring-2 focus:ring-blue-500"
            />
          </div>

          {/* Borrowed Books */}
          <BorrowedBooks books={borrowedBooks} />

          {/* Recently Returned */}
          <div className="recently-returned space-y-2">
            <h2 className="text-xl font-semibold">Recently Returned</h2>
          </div>

          {/* Reading Stats */}
          <div className="reading-stats"></div>
        </div>

        <div className="">
          <aside className="hidden sm:block sidebar p-4 space-y-4 border-l">
            <div className="recommended-books space-y-2">
              <h2 className="text-xl font-semibold">Recommended For You</h2>
              <ul className="space-y-2">
                <li className="px-2 py-1 hover:bg-gray-100 rounded">Book 1</li>
                <li className="px-2 py-1 hover:bg-gray-100 rounded">Book 2</li>
              </ul>
            </div>

            {/* Events or Classes */}
            <div className="upcoming-events space-y-2">
              <h2 className="text-xl font-semibold">Upcoming Events</h2>
            </div>

            {/* News & Updates */}
            <div className="library-news space-y-2">
              <h2 className="text-xl font-semibold">News & Updates</h2>
            </div>
          </aside>
        </div>
      </div>
    </div>
  );
};

export default UserDashboard;
