import React from "react";
import { useJwtToken } from "../../../App";

const StaffDashboard = () => {
  const jwtToken = useJwtToken();

  return <div>StaffDashboard</div>;
};

export default StaffDashboard;
