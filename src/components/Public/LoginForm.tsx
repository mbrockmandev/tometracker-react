import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { isValidEmail, isValidPassword } from "../../utils/validators";
import { UserContext, UserContextType } from "../../App";
import { useJwtToken } from "./../../App";

const LoginForm = () => {
  const { jwtToken, setJwtToken } = useJwtToken();
  const { user, setUser } = useContext<UserContextType>(UserContext);
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    if (jwtToken !== "") {
      if (user.role === "admin") {
        console.log("admin!");
        navigate("/admin/dashboard");
      }
      if (user.role === "staff") {
        console.log("staff!");
        navigate("/staff/dashboard");
      }
      if (user.role === "user") {
        console.log("user!");
        navigate("/users/dashboard");
      }
      return;
    }
  }, [user, jwtToken, navigate]);

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const handlePasswordVisibleClick = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();

    try {
      if (!isValidEmail(email)) {
        throw new Error("Invalid email");
      }

      if (!isValidPassword(password)) {
        throw new Error("Invalid password");
      }

      const reqOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          credentials: "include",
        },
        body: JSON.stringify({
          email,
          password,
        }),
      };
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND}login`,
        reqOptions,
      );

      const data = await res.json();

      setUser({
        email: data.user_info["email"],
        role: data.user_info["role"],
        jwt: data["access_token"],
      });
      setJwtToken(data["access_token"]);
    } catch (err) {
      setError(err.message);
      if (error !== "") {
        return;
      }
      setTimeout(() => {
        setError("");
      }, 5000);
    }
  };

  return (
    <div>
      <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <div className="mx-auto max-w-lg">
          <h1 className="text-center text-2xl font-bold text-green-700 sm:text-3xl">
            TomeTracker
          </h1>

          <p className="mx-auto mt-4 max-w-md text-center text-gray-500">
            Are you ready to explore the world?
          </p>

          <form
            action=""
            className="mb-0 mt-6 space-y-4 rounded-lg p-4 shadow-lg shadow-green-300/50 sm:mt-8 sm:p-6 lg:p-8">
            <p className="text-center text-lg font-medium">
              Sign in to your account
            </p>

            <div>
              <label
                htmlFor="email"
                className="sr-only">
                Email
              </label>

              <div className="relative">
                <input
                  type="email"
                  className="w-full rounded-lg border-green-200 p-4 pe-12 text-sm shadow-sm"
                  placeholder="Email"
                  onChange={handleEmailChange}
                />

                {/* email icon */}
                <span className="absolute inset-y-0 end-0 grid place-content-center px-4">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 text-gray-400"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"
                    />
                  </svg>
                </span>
              </div>
            </div>

            <div>
              <label
                htmlFor="password"
                className="sr-only">
                Password
              </label>

              <div className="relative pt-4">
                <input
                  id="confirmPassword"
                  type={showPassword ? "text" : "password"}
                  onChange={handlePasswordChange}
                  className="w-full rounded-lg border-green-200 p-4 pe-12 text-sm shadow-sm shadow-green-300"
                  placeholder="Confirm Password"
                />

                <span
                  onClick={handlePasswordVisibleClick}
                  className="absolute right-4 top-11 transform -translate-y-1/2 cursor-pointer">
                  {showPassword ? "🙈" : "👀"}
                </span>
              </div>
            </div>

            <button
              type="submit"
              className="block w-[35%] bg-green-200 rounded-lg bg-secondary px-5 py-3 text-sm font-medium text-black w-[25%] mx-auto"
              onClick={handleSubmit}>
              Sign in
            </button>

            <p className="text-center text-sm text-gray-500">
              No account?{" "}
              <Link
                className="hover:underline text-green-700 hover:text-green-500"
                to="/register">
                Sign up
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
