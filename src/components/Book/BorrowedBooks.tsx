import React, { useState } from "react";
import BookList from "./BookList";
import Book from "../../utils/models/Book";

export default function BorrowedBooks(props) {
  const [borrowedBooks, setBorrowedBooks] = useState<Book[]>([]);

  return (
    <>
      <div>
        <BookList books={borrowedBooks} />
      </div>
    </>
  );
}
