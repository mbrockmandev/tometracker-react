import React from "react";
import BookCard from "./BookCard";

const BookList = ({ books }) => {
  return (
    <>
      {books &&
        books.map((b) => (
          <>
            <BookCard
              key={`${b.title}-${b.summary}`}
              title={b.title}
              summary={b.summary}
              thumbnail={b.thumbnail}
            />
          </>
        ))}
    </>
  );
};

export default BookList;
