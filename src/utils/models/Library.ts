export type Library = {
  id: number;
  name: string;
};

export default Library;
