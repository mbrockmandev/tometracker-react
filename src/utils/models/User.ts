type User = {
  firstName: string;
  lastName: string;
  email: string;
  jwtToken: string;
};

export default User;
