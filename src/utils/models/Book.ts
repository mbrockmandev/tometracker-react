type Book = {
  id: number;
  title: string;
  author: string;
  description: string;
  thumbnail: string;
};

export default Book;
