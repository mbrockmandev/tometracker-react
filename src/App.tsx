import * as React from "react";
import { createContext, useCallback, useEffect, useState } from "react";
import { Outlet, useNavigate, useOutletContext } from "react-router-dom";

import Footer from "./components/Common/Footer";
import Header from "./components/Common/Header";
import { OutletContextType } from "./utils/interfaces";
import Book from "./utils/models/Book";

export const UserContext = createContext<UserContextType>(null);

interface User {
  email: string;
  role: string;
  jwt: string;
}

const App = () => {
  const navigate = useNavigate();
  const [jwtToken, setJwtToken] = useState<string>("");
  const [alertMessage, setAlertMessage] = useState<string>("");
  const [alertClassName, setAlertClassName] = useState<string>("");
  const [tickInterval, setTickInterval] = useState<NodeJS.Timer>();
  const [user, setUser] = useState<User>({
    email: "",
    role: "",
    jwt: "hi",
  });

  const logout = () => {
    const requestOptions: RequestInit = {
      method: "GET",
      credentials: "include",
    };

    fetch(`${process.env.REACT_APP_BACKEND}/logout`, requestOptions)
      .catch((err) => {
        console.log("error logging out" + err);
      })
      .finally(() => {
        setJwtToken("");
        toggleRefresh(false);
      });

    navigate("/login");
  };

  const toggleRefresh = useCallback(
    (status: boolean) => {
      if (status) {
        let i = setInterval(() => {
          const requestOptions: RequestInit = {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
            },
            credentials: "include",
          };

          fetch(`${process.env.REACT_APP_BACKEND}refresh`, requestOptions)
            .then((response) => {
              if (!response.ok) {
                throw new Error("Network response was not ok");
              }

              return response.json();
            })
            .then((data) => {
              if (data["access_token"]) {
                console.log("user is logged in");
                setJwtToken(data["access_token"]);
              }
            })
            .catch((error) => {
              console.log("user is not logged in ", error);
            });
        }, 600000);
        setTickInterval(i);
      } else {
        setTickInterval(null);
        clearInterval(tickInterval);
      }
    },
    [tickInterval],
  );

  useEffect(() => {
    if (jwtToken === "") {
      const requestOptions: RequestInit = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
      };

      fetch(`${process.env.REACT_APP_BACKEND}refresh`, requestOptions)
        .then((response) => {
          if (!response.ok) {
            console.error(response);
            throw new Error("Network response was not ok");
          }
          return response.json();
        })
        .then((data) => {
          if (data["access_token"]) {
            console.log("user is logged in");
            setJwtToken(data["access_token"]);
            toggleRefresh(true);
          }
        })
        .catch((error) => {
          console.error("error: ", error);
        });
    }
  }, [jwtToken, toggleRefresh]);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <div className="background-root">
        <Header />
        <Outlet
          context={
            {
              jwtToken,
              setJwtToken,
              setAlertMessage,
              setAlertClassName,
              toggleRefresh,
            } satisfies OutletContextType
          }
        />
        <Footer />
      </div>
    </UserContext.Provider>
  );
};

export interface UserContextType {
  user: User;
  setUser: React.Dispatch<React.SetStateAction<User>>;
}

export function useJwtToken() {
  return (
    useOutletContext<OutletContextType>() || {
      jwtToken: "",
      setJwtToken: () => {},
      setAlertMessage: () => {},
      setAlertClassName: () => {},
      toggleRefresh: () => {},
    }
  );
}

export default App;
